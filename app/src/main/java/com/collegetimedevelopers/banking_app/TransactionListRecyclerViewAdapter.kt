package com.collegetimedevelopers.banking_app

import android.content.Context
import android.graphics.Color
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.collegetimedevelopers.banking_app.database.Database
import com.collegetimedevelopers.banking_app.database.TransactionEntity
import com.collegetimedevelopers.banking_app.database.UserEntity
import org.w3c.dom.Text

/**
 * @author Prateek Kumar Rai,Sarthak Gupta
 */

class TransactionListRecyclerViewAdapter(private val context: Context, private val items: List<TransactionEntity>, private val id : Int): RecyclerView.Adapter<TransactionListRecyclerViewAdapter.TransactionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val view=
            LayoutInflater.from(parent.context).inflate(R.layout.transaction_ui , parent,false)
        return TransactionViewHolder(view)
    }
    override fun getItemCount(): Int {
        return items.size
    }
    /**
     * @author Sarthak Gupta
     */

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val currentItem =items[position]
        val user1 = GetUserByIdAsyncTask(context, currentItem.transactionFromId).execute().get()
        val user2 = GetUserByIdAsyncTask(context, currentItem.transactionToId).execute().get()
        val type : String = when(currentItem.transactionFromId == id){
            true -> "sent to"
            false -> "received from"
        }
        /**
         * @author Ishan Khandelwal
         */
        if(currentItem.transactionFromId == id)
        {
            holder.amountTxt.setTextColor(Color.RED)
        }
        else{
            holder.amountTxt.setTextColor(Color.GREEN)
        }

        holder.senderTxt.text = when(type){
            "sent to" -> user1.name
            else -> user2.name
        }
        holder.receiverTxt.text = when(type){
            "sent to" -> user2.name
            else -> user1.name
        }
        val rupee="₹ "
        val transactedAmt = rupee+currentItem.amount.toString()
        holder.amountTxt.text = transactedAmt
        holder.typeTxt.text = type

    }
    open inner class TransactionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val senderTxt = view.findViewById<TextView>(R.id.sender)
        val receiverTxt = view.findViewById<TextView>(R.id.receiver)
        val amountTxt = view.findViewById<TextView>(R.id.amount_transact)
        val typeTxt = view.findViewById<TextView>(R.id.sent)
    }

    class GetUserByIdAsyncTask(context: Context, private val id : Int) : AsyncTask<Void, Void, UserEntity>(){
        val db = Room.databaseBuilder(context, Database::class.java, "transaction-db").build()
        override fun doInBackground(vararg params: Void?): UserEntity {
            return db.userDao().getUserWithId(id)
        }

    }
}