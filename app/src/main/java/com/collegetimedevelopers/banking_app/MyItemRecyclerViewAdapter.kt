package com.collegetimedevelopers.banking_app

import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.inflate
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.navigation.Navigation.findNavController
import com.collegetimedevelopers.banking_app.database.UserEntity

/**
 * @author Prateek Kumar Rai, Ishan Khandelwal
 */

class MyItemRecyclerViewAdapter(
    private val values: List<UserEntity>
) : RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder>() {
     var  id = 0
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val user: TextView = view.findViewById(R.id.content)
        val mylayout:LinearLayout = view.findViewById(R.id.viewmodel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.fragment_item2,
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        id=position
        holder.user.text = item.name
        val bundle = Bundle()
        bundle.putInt("id", item.id)
        bundle.putString("name", item.name)
        bundle.putString("email", item.email)
        bundle.putInt("balance", item.balance)
        bundle.putInt("accountNumber", item.accountNumber)
        holder.mylayout.setOnClickListener {
            findNavController(it).navigate(R.id.action_itemFragment_to_cutomerUI, bundle)
        }
    }

    override fun getItemCount(): Int = values.size



}