package com.collegetimedevelopers.banking_app

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.room.Room
import com.collegetimedevelopers.banking_app.database.Database
import com.collegetimedevelopers.banking_app.database.UserEntity

/**
 * A fragment representing a list of Items.
 */
/**
 * @author Ishan Khandelwal
 */
class ItemFragment : Fragment() {

    private var columnCount = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_item_list, container, false)

        /**
         * @author Prateek Kumar Rai
         */

        val userDb: Array<UserEntity> = arrayOf(
                UserEntity(0, "Prateek Kumar Rai", "rprateek254@gmail.com", 10000, 1001),
                UserEntity(1, "Ishan Khandelwal", "abc@gmail.com", 10000, 1002),
                UserEntity(2, "Sarthak Gupta", "abc@gmail.com", 10000, 1003),
                UserEntity(3, "Ashish Kumar", "abc@gmail.com", 10000, 1004),
                UserEntity(4, "Jhon Doe", "abc@gmail.com", 10000, 1005),
                UserEntity(5, "Lionel Messi", "abc@gmail.com", 10000, 1006),
                UserEntity(6, "Cristiano Ronaldo", "abc@gmail.com", 10000, 1007),
                UserEntity(7, "Virat Kohli", "abc@gmail.com", 10000, 1008),
                UserEntity(8, "Rohit Sharma", "abc@gmail.com", 10000, 1009),
                UserEntity(9, "Pardeep Narwal", "abc@gmail.com", 10000, 1010),
                UserEntity(10, "MS Dhoni", "abc@gmail.com", 10000, 1011)
            )

        val list = context?.let { UsersAsyncTask2(it).execute().get() }

        if(list?.size == 0){
            for(i in userDb) {
                val async = context?.let { UsersAsyncTask(it, i).execute().get() }
            }
        }

        val userList : List<UserEntity>

        val async = context?.let { UsersAsyncTask2(it).execute() }
        userList = if (async != null) {
            async.get()
        } else{
            List(1){ UserEntity(0,"0", "0", 0, 0) }
        }


        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = MyItemRecyclerViewAdapter(userList)
            }
        }

        return view
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            ItemFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }

    /**
     * @author Prateek Kumar Rai
     */

    class UsersAsyncTask(context: Context,private val userEntity: UserEntity) : AsyncTask<Void, Void, Boolean>(){
        val db = Room.databaseBuilder(context, Database::class.java, "transaction-db").build()

        override fun doInBackground(vararg params: Void?): Boolean {
            db.userDao().newUser(userEntity)
            return true
        }

    }

    class UsersAsyncTask2(context: Context) : AsyncTask<Void, Void, List<UserEntity>>(){
        val db = Room.databaseBuilder(context, Database::class.java, "transaction-db").build()

        override fun doInBackground(vararg params: Void?): List<UserEntity> {
            return db.userDao().getAllUsers()
        }

    }
}

