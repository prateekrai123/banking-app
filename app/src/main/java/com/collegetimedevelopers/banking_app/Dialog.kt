package com.collegetimedevelopers.banking_app

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.room.Room
import com.collegetimedevelopers.banking_app.database.Database
import com.collegetimedevelopers.banking_app.database.TransactionEntity
import com.collegetimedevelopers.banking_app.database.UserEntity
import java.util.*
import android.widget.EditText
import android.widget.Toast.LENGTH_SHORT
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.text.Editable

import android.text.TextWatcher


/**
 * @author Prateek Kumar Rai, Ishan Khandelwal
 */

class Dialog : BottomSheetDialogFragment(){
    private var id : Int? = null
    private var balance : Int? = null

    lateinit var btnPay : Button
    lateinit var amountText : EditText
    lateinit var idText : EditText
    lateinit var btnCancel : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id = it.getInt("id")
            balance = it.getInt("balance")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.add_transaction_dialog,container,false)


//        val Amount=view.findViewById<EditText>(R.id.amount)

        btnPay = view.findViewById(R.id.paybutton)
        amountText = view.findViewById(R.id.amount)
        idText = view.findViewById(R.id.accnumber)
        btnCancel = view.findViewById(R.id.action_cancel)

        amountText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (s.toString().length == 1 && s.toString().startsWith("0")) {
                    s.clear()
                }
            }
        })
        btnPay.setOnClickListener {
            val checkNullability =
                idText.text.toString().isNotEmpty() && amountText.text.toString().isNotEmpty()
            if (checkNullability) {
                val idWatcher = idText.text.toString().toInt()
                val updatedAmount = balance?.minus(amountText.text.toString().toInt())
                if(idWatcher in 0..10) {
                    if (id != idText.text.toString().toInt()) {
                        if (updatedAmount != null) {
                            if (updatedAmount >= 0) {
                                val transactionData =
                                    id?.let { it1 ->
                                        TransactionEntity(
                                            UUID.randomUUID().toString(),
                                            it1,
                                            idText.text.toString().toInt(),
                                            amountText.text.toString().toInt()
                                        )
                                    }
                                val async =
                                    context?.let { it1 ->
                                        transactionData?.let { it2 ->
                                            NewTransactionAsyncTask(
                                                it1,
                                                it2
                                            ).execute().get()
                                        }
                                    }
                                if (async == true) {
                                    val newBalance =
                                        balance?.minus(amountText.text.toString().toInt())
                                    val user: UserEntity? = context?.let {
                                        id?.let { it1 ->
                                            GetUserAsyncTask(it, it1).execute().get()
                                        }
                                    }
                                    user?.balance = newBalance!!
                                    context?.let { it1 ->
                                        if (user != null) {
                                            UpdateAmountAsyncTask2(it1, user).execute().get()
                                        }
                                    }
                                    val user2: UserEntity? = context?.let {
                                        idText.text.toString().toInt()
                                            .let { it1 ->
                                                GetUserAsyncTask(it, it1).execute().get()
                                            }
                                    }

                                    val user2balance =
                                        user2?.balance?.plus(amountText.text.toString().toInt())
                                    if (user2balance != null) {
                                        user2.balance = user2balance
                                    }

                                    context?.let { it1 ->
                                        if (user2 != null) {
                                            UpdateAmountAsyncTask2(it1, user2).execute().get()
                                        }
                                    }
                                    Toast.makeText(
                                        context,
                                        "Transaction completed",
                                        Toast.LENGTH_LONG
                                    )
                                        .show()
                                    val bundle = Bundle()
                                    id?.let { it1 -> bundle.putInt("id", it1) }
                                    findNavController().navigate(
                                        R.id.action_dialog_to_cutomerUI,
                                        bundle
                                    )
                                }
                            } else {
                                Toast.makeText(context, "Balance is low", Toast.LENGTH_LONG).show()
                            }
                        } else {
                            Toast.makeText(context, "Balance not available", Toast.LENGTH_LONG)
                                .show()
                        }
                    }
                    else {
                        Toast.makeText(context, "You cannot pay yourself!", LENGTH_SHORT).show()
                    }
                }
                else {
                    idText.error = "Out of bounds 0<=id<=10"
                }
            }
            else{
                if(idText.text.toString().isEmpty()){
                    idText.error = "Should not be empty"
                }
                else{
                    amountText.error = "Should not be empty"
                }
            }
        }
        btnCancel.setOnClickListener {
            dismiss()
        }
        return view
    }

    class NewTransactionAsyncTask(context : Context, private val transactionEntity: TransactionEntity) : AsyncTask<Void, Void, Boolean>(){
        val db = Room.databaseBuilder(context, Database::class.java, "transaction-db").build()

        override fun doInBackground(vararg params: Void?): Boolean {
            db.transactionDao().newTransaction(transactionEntity)
            return true
        }

    }

    class UpdateAmountAsyncTask2(context: Context, private val user : UserEntity) : AsyncTask<Void, Void, Boolean>(){
        val db = Room.databaseBuilder(context, Database::class.java, "transaction-db").build()
        override fun doInBackground(vararg params: Void?): Boolean {
            db.userDao().updateUser(user)
            return true
        }

    }

    class GetUserAsyncTask(context : Context, private val id : Int) : AsyncTask<Void, Void, UserEntity>(){
        val db = Room.databaseBuilder(context, Database::class.java, "transaction-db").build()
        override fun doInBackground(vararg params: Void?): UserEntity {
            return db.userDao().getUserWithId(id)
        }

    }
}