package com.collegetimedevelopers.banking_app

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.collegetimedevelopers.banking_app.database.Database
import com.collegetimedevelopers.banking_app.database.TransactionEntity
import com.collegetimedevelopers.banking_app.database.UserEntity
import kotlinx.android.synthetic.main.fragment_cutomer_u_i.*
import kotlinx.android.synthetic.main.fragment_item2.*

/**
 * @author Prateek Kumar Rai
 */

class CutomerUI : Fragment() {

    private var id : Int? = null
    private var columnCount = 1

    lateinit var nameText : TextView
    lateinit var emailText : TextView
    lateinit var balanceText : TextView
    lateinit var idText : TextView
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id = it.getInt("id")
            columnCount = it.getInt(ItemFragment.ARG_COLUMN_COUNT)
        }

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_cutomer_u_i, container, false)
        nameText = view.findViewById(R.id.customer_name)
        emailText = view.findViewById(R.id.cus_email)
        balanceText = view.findViewById(R.id.balance)
        idText = view.findViewById(R.id.txtId)
        recyclerView = view.findViewById(R.id.recyclerView)

        val user = context?.let { id?.let { it1 -> GetUserWithIDAsyncTask(it, it1).execute().get() } }

        nameText.text = user?.name
        "Email   :   ${user?.email}".also { emailText.text = it }
        "Balance   :  ₹ ${user?.balance}".also { balanceText.text = it }
        "ID : $id".also { idText.text = it }


        /**
         * Transaction list work goes here
         */

        val allTransactions = context?.let { GetTransactionAsyncTask(it).execute().get() }
        var transactions = mutableListOf<TransactionEntity>()
        allTransactions?.forEach {
            if(it.transactionFromId == id){
                transactions.add(it)
            }else{
                if(it.transactionToId == id){
                    transactions.add(it)
                }
            }
        }
        val myTransactions : List<TransactionEntity> = transactions

        /**
         * @author Ishan Khandelwal
         */
        view.findViewById<TextView>(R.id.new_transaction_button).setOnClickListener{
            val bundle = Bundle()
            id?.let { it1 -> bundle.putInt("id", it1) }
            user?.balance?.let { it1 -> bundle.putInt("balance", it1) }
            findNavController().navigate(R.id.action_cutomerUI_to_dialog, bundle)
        }

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter =
            context?.let { id?.let { it1 ->
                /**
                 * @author Ishan Khandelwal
                 */
                TransactionListRecyclerViewAdapter(it, myTransactions.reversed(),
                    it1
                )
            } }

        return view
    }


    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CutomerUI().apply {
                arguments = Bundle().apply {
                    putInt(ItemFragment.ARG_COLUMN_COUNT, columnCount)
                }
            }
    }

    class GetTransactionAsyncTask(context : Context) : AsyncTask<Void, Void, List<TransactionEntity>>() {
        val db = Room.databaseBuilder(context, Database::class.java, "transaction-db").build()
        override fun doInBackground(vararg params: Void?): List<TransactionEntity> {
            return db.transactionDao().getAllTransactions()
        }

    }

    class GetUserWithIDAsyncTask(context : Context, private val id : Int) : AsyncTask<Void, Void, UserEntity>(){
        val db = Room.databaseBuilder(context, Database::class.java, "transaction-db").build()
        override fun doInBackground(vararg params: Void?): UserEntity {
            return db.userDao().getUserWithId(id)
        }

    }
}


