package com.collegetimedevelopers.banking_app.database

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * @author Prateek Kumar Rai
 * @date 06 September 2021
 * */

@Database(entities = [TransactionEntity::class, UserEntity::class], version = 1)
abstract class Database : RoomDatabase() {

    abstract fun transactionDao() : TransactionDao

    abstract fun userDao(): UserDao

}