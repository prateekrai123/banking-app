package com.collegetimedevelopers.banking_app.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

/**
 * @author Prateek Kumar Rai
 * @date 06 September 2021
 * */

@Dao
interface TransactionDao {

    @Insert
    fun newTransaction(transactionEntity: TransactionEntity)

    @Query(value = "SELECT * FROM transactions")
    fun getAllTransactions(): List<TransactionEntity>

    @Query(value = "SELECT * FROM transactions WHERE transaction_from_id=:id")
    fun getSentTransactionsById(id : Int): List<TransactionEntity>

    @Query(value = "SELECT * FROM transactions WHERE transaction_to_id=:id")
    fun getReceivedTransactionsById(id : Int): List<TransactionEntity>

}