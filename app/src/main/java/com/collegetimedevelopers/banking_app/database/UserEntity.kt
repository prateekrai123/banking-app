package com.collegetimedevelopers.banking_app.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author Prateek Kumar Rai
 */

@Entity(tableName = "users")
data class UserEntity(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "name")val name: String,
    @ColumnInfo(name = "email")val email: String,
    @ColumnInfo(name = "balance") var balance: Int,
    @ColumnInfo(name = "account_number")val accountNumber : Int
)