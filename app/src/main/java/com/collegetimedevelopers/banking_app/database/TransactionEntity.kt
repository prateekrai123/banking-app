package com.collegetimedevelopers.banking_app.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author Prateek Kumar Rai
 * @date 06 September 2021
 * */

@Entity (tableName= "transactions")
data class TransactionEntity(
    @PrimaryKey val id : String,
    @ColumnInfo(name = "transaction_from_id")val transactionFromId : Int,
    @ColumnInfo(name = "transaction_to_id")val transactionToId: Int,
    @ColumnInfo(name = "value")val amount : Int
)