package com.collegetimedevelopers.banking_app.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

/**
 * @author Prateek Kumar Rai
 */

@Dao
interface UserDao {

    @Insert
     fun newUser(userEntity: UserEntity)

    @Query(value = "SELECT * FROM users")
    fun getAllUsers(): List<UserEntity>

    @Query(value = "SELECT * from users WHERE id=:id")
    fun getUserWithId(id : Int) : UserEntity

    @Update
    fun updateUser(userEntity: UserEntity)

}