package com.collegetimedevelopers.banking_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager

/**
 * @author Ashish Kumar
 */

class SplashScreenActivity : AppCompatActivity() {

    private val splashscreentimeout : Long = 2500

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        supportActionBar?.hide()
        Handler().postDelayed({
                   startActivity(Intent(this,MainActivity::class.java))
            finish()
        },splashscreentimeout)
    }

}
